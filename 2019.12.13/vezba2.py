USER        = 0b0001
ADMIN       = 0b0010
SUPERADMIN  = 0b0100

MASK = ADMIN | SUPERADMIN

user = int(input("Enter role: "))

is_allowed = (user & MASK) != 0

print(is_allowed)