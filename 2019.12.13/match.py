import random

FIRST_HALF  = 1
PAUSE       = 2
SECOND_HALF = 3
MATCH_ENDED = 4

status      = random.randint(1,4)

if status == FIRST_HALF:
    print("Prvo poluvreme u toku")
elif status == PAUSE:
    print("Poluvreme")
elif status == SECOND_HALF:
    print("Drugo poluvreme")
else:
    print("Kraj")