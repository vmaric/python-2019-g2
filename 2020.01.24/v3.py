class Putovanje: 
    def prevoz(self):
        return 2
    def izracunaj(self):
        self.cena = self.prevoz() * self.razdaljina
        print(self.cena)
    def __init__(self,destinacija,razdaljina):
        self.cena = 0
        self.razdaljina = razdaljina
        self.destinacija = destinacija
    def show(self):
        print(self.cena,self.destinacija)
 
class Voz(Putovanje):
    def prevoz(self):
        return 3

class Avion(Putovanje):
    def prevoz(self):
        return 10

p = Voz("London",200)
p1 = Avion("London",200)
p.izracunaj()
p1.izracunaj()