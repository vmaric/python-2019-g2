class Tag:
    def draw_tag(self,tagname,tagcontent,style=""):
        return "<" + tagname + " " + style +">"+tagcontent+"</"+tagname+">"

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y 

    def __add__(self,desna):
        pt2 = Point(self.x+desna.x,self.y+desna.y)
        return pt2

class Megapoint(Point,Tag):
    def __str__(self): 
        style = f"style='width:10px;height:10px;background-color:red;position:absolute;left:{self.x}px;top:{self.y}px'"
        return self.draw_tag("div","tralala",style)

mp = Megapoint(2,3)
print(mp)