import random
import abc
class Game(abc.ABC):
    @abc.abstractmethod
    def playround(self): pass
    def spin(self):
        print("Spin is running")
        print("Depositing")
        self.playround()

class Rulet(Game):
    def playround(self):
        print("The number is ",random.randint(0,32))

class Slot(Game):
    signs = ("Apple","Cherry","Star","Pear","Banana")
    def playround(self):
        print("Sign is",self.signs[random.randint(0,4)])

class DonkeyKong(Game):
    def playround(self):
        print("Titutututututututtuutrararadasf")

s = DonkeyKong()
s.spin()