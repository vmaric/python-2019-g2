class Point:
    def __init__(self):
        self.__x = 0
        self.__y = 0
    @property
    def x(self):
        return self.__x
    @x.setter
    def x(self,val):
        if isinstance(val,int):
            self.__x = val
    def move(self):
        print(f"Moving to {self.__x} {self.__y}")

pt = Point()
pt.x = "Vlada"

pt.move()