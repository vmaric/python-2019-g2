import socket
import selectors
import time
import random

server = socket.socket()
server.bind(("localhost",8005))
server.listen()
server.setblocking(0)

selobjekat = selectors.DefaultSelector()

klijenti = []

def citanje(sock):
    poruka = sock.recv(1024)
    if not poruka:
        klijenti.remove(sock)

def pristup(sock):
    klijent,_ = sock.accept()
    klijent.setblocking(0)
    klijenti.append(klijent)
    selobjekat.register(klijent,selectors.EVENT_READ,citanje) 
selobjekat.register(server,selectors.EVENT_READ,pristup)

def proveri_vreme():
    temperatura = random.randint(-20,20)
    for sock in klijenti:
        sock.send(str(temperatura).encode('utf-8'))
cnt = 0
while True: 
    lista_soketa = selobjekat.select(0.001) 
    for sock in lista_soketa:
        sock[0].data(sock[0].fileobj)
    cnt+=1
    if cnt%500 == 0:
        proveri_vreme()