import asyncio
import time

# async def prognoza():
#     print("vreme:")
#     await asyncio.sleep(2)
#     print("suncano")

# async def vetar():
#     print("Vetar:")
#     await asyncio.sleep(2)
#     print("Siba jako")    

# async def f(): 
#     task1 = asyncio.create_task(prognoza())
#     task2 = asyncio.create_task(vetar())
#     await task1
#     await task2

async def citanje_pisanje(citanje,pisanje):
    pisanje.write(b"Hello man, konacno pricamo!")
    while True:
        await asyncio.sleep(1)
        pisanje.write(str(time.time()).encode('utf-8'))
    #await citanje.read()
   # print()

f = asyncio.start_server(citanje_pisanje,host="localhost",port=8005)

petlja = asyncio.get_event_loop() 
petlja.run_until_complete(f)
petlja.run_forever()