import hashlib 
 
username = input("Username:")
password = input("Password:")

md5 = hashlib.sha256(password.encode('utf-8'))
password = md5.hexdigest()

print(password)

with open("users","r") as fajl:
    linija = fajl.readline()
    while linija: 
        korisnik = linija.strip().split(",")
        if korisnik[0] == username and korisnik[1] == password:
            print(f"Dobro dosao" if korisnik[3] == "m" else "Dobro nam dosla")
            break
        linija = fajl.readline()
    else:
        print("Nepostojeci korisnik")
