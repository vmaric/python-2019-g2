suma        = 0
limit       = 3

while True:
    jedan = input("Unesi broj:")
    if not jedan: 
        break
    if not jedan.isnumeric():
        if limit <= 0:
            print("E, sad si preterao")
            break
        print("Momak...samo brojevi, molim")
        limit-=1
        continue

    jedan = int(jedan)
    suma  += jedan

print("Konacan zbir ", suma)
