dogs        = []
UNOS        = 1
BRISANJE    = 2
SVI         = 3
IZLAZ       = 4

while True:

    print("1. Unos, 2. Brisanje, 3. Svi psi, 4. Izlaz")
    
    komanda = input("Unesite komandu: ")
    if not komanda.isnumeric():
        print("Invalid command")
        continue
    else:
        komanda = int(komanda)
        if komanda not in [UNOS,BRISANJE,SVI,IZLAZ]:
            print("Invalid command code")
            continue 
    if komanda == UNOS:
        #Unos 
        unos = input("Unesite naziv psa: ")
        dogs.append(unos)
    elif komanda == BRISANJE:
        #Izbacivanje
        unos = input("Unesite naziv psa: ") 
        dogs.remove(unos)
        print(unos," obrisano")
    elif komanda == SVI:
        print("Lista svih pasa")
        #listanje
        for dog in dogs:
            print(dog)
    else:
        exit(0)