import socket


broj_akumulatora = 10

server = socket.socket()
server.bind(("localhost",9999))
server.listen()

while True:
    client,addr = server.accept()

    client.send((f"Akumulatora na stanju {broj_akumulatora}").encode("utf-8"))

    komada = client.recv(100)

    if not komada:
        print("Prijatelj nista nije uneo")

    komada = komada.decode("utf-8")
    komada = int(komada)
    broj_akumulatora -= komada
    client.send((f"Akumulatora na stanju {broj_akumulatora}").encode("utf-8"))