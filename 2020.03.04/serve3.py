import time
import socket
import select

server = socket.socket()
server.bind(("localhost",9999))
server.listen() 
server.setblocking(0) 

slusanje = [server]
pricanje = []
problem  = []

while True:
    s,p,pr = select.select(slusanje,pricanje,problem)
    for sock in s:
        if sock == server:
            client,addr = sock.accept()
            client.setblocking(0)
            slusanje.append(client)
        else:
            msg = sock.recv(1024) 
            print(msg)
            if not msg:
                sock.close()
                slusanje.remove(sock) 
    print(slusanje)