'''
Ovo je klasa skola. Ona radi sa ocenama.
Ocene koje su prihvatljive su: 1,2,3,4 i 5
Ukoliko ubacis neprihvatljivu ocenu
metod izbacuje izuzetke

NeispravnaOcenaException,NeispravnaOpisnaOcenaException
'''
class skola: 
    def oceni(self,ocena):
        if ocena not in [1,2,3,4,5]:
            raise NeispravnaOcenaException()
        print(f"ocena je {ocena}")
    def opisnaocena(self,ocena):
        if not ocena in 'ABC':
            raise NeispravnaOpisnaOcenaException()
        print(f"opisna ocena je {ocena}")
        
class NeispravnaOcenaException(Exception): pass
class NeispravnaOpisnaOcenaException(Exception): pass