import urllib.request 
# res = urllib.request.urlopen("http://www.dota2.com/international/overview/")
# sadrzaj = res.read().decode("utf-8")
# print(sadrzaj) 

#PRIMER POSTO FORMATIRANJE
# txt = "Total kills: %d, Total minions: %.2f"
# total = 15
# totalm = 120.165
# title = txt % (total,totalm)
# print(title)

#PRIMER STRING FORMAT METODA
# txt = "Total kills: {1:.2f}, Total minions: {0}"
# total = 15
# totalm = 120.165
# title = txt.format(total,totalm)
# print(title)

#PRIMER EVALUACIJA - F STRING
total = 15
totalm = 120.165
txt = f"Total kills: {total}, Total minions: {totalm:.2f}"
print(txt)
