stripovi = [
    {
        "naziv":"Alan Ford",
        "zanr":"Satira",
        "autori":[
            {"ime":"Max","prezime":"Bunker"},
            {"ime":"Magnus","prezime":"neki tamo"}
        ],
    },
    {
        "naziv":"Dilan Dog",
        "zanr":"Horor",
        "autori":[
            {"ime":"Aleksandar","prezime":"Safonov"}
        ],
    }
]


for strip in stripovi:
    print(strip["naziv"])
    print(strip["zanr"])
    print("Autori:")
    for autor in strip["autori"]:
        print(autor["ime"],autor["prezime"])
