kolaci = [
    ["baklava",10],
    ["cupavci",2],
    ["bananice",8],
    ["Aljonka",9],
    ["Krempita",10],
    ["Miska",5]
]

l = len(kolaci)
while True:
    has_sort = False 
    for i in range(1,l):
        if kolaci[i][1] < kolaci[i-1][1]:
            tmp = kolaci[i]
            kolaci[i] = kolaci[i-1]
            kolaci[i-1] = tmp
            has_sort = True
    if not has_sort:
        break

for kolac in kolaci:
    print(kolac[0],"ocena",kolac[1])