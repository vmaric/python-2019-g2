def gpozdrav(ime,*a,**k):  
    jezik = "en" if "ll" not in k else k["ll"]
    lang = {"en":"Hello","sr":"Cao","ru":"Privet"}
    rezultat = lang[jezik]+" "+ime
    return rezultat

res = gpozdrav("Vlada","ru",ll="ru")
print(res)