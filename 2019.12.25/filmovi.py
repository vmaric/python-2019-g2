genres = ["Comedy","Triller","Fantasy"]
actors = ["Michael Kane","Leonardo di Caprio","Robert de Niro","Al pacino","Henry Cavil","Johnny Knoxwille"]
filmovi = [
    ["Inception",0,[0,1]],
    ["Irishman",1,[2,3]],
    ["The witcher",2,[4]],
    ["The Godfather",1,[2,3]],
    ["Bad Granpa",0,[5]],
    ["The wolf from wall street",1,[1]]
]

for film in filmovi:
    print(film[0])
    print("Genre: ",genres[film[1]])
    print("Starring: ")
    for glumac in film[2]:
        print(actors[glumac],end=" ")
    print()