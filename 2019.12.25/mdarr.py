paketi = [
    ["Starter",500,["500 mb","Neograniceno poruka"],50],
    ["Prevaranter",550,["1GB","Neograniceno minuta"],100],
    ["Preskupanter",10000,["Roaming","Uredjaj","Sve neograniceno"],99999999],
    ["Jadnik",100,["Maketa uredjaja"],1]
]
for i in paketi:
    print("Paket: " , i[0] , "Cena: ", i[1], "Minuta: ", i[3]) 
    print("Dodaci:")
    for dodatak in i[2]:
        print(dodatak,end=" ")
    print()
    print("---------------------------------------------------")