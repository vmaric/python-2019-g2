forbiden_weapons = ["gun","bomb","rifle"]
weapons = []

while True:
    new_weapon = input("Enter weapon: ") 
    if not new_weapon:
        break
    if new_weapon in forbiden_weapons:
        print("You can not use this weapon")
    else:
        weapons.append(new_weapon) 
    print(weapons)
print(weapons)


