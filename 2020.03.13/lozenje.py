import matplotlib.pyplot as plt
import mysql.connector as conn

db = conn.connect(host="localhost",database="g2_hospital",user="root",passwd="")

y = []
x = []
cur = db.cursor()

cur.execute("select odeljenje,sum(age) from pacijenti group by odeljenje")

for red in cur.fetchall():
    x.append(red[0])
    y.append(red[1])

db.close()

plt.plot(x,y)

plt.show()

