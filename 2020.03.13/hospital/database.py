import mysql.connector as connector
import hospital.config as config
import hospital.odeljenje

class Database:
    def __getconnection(self):
        return connector.connect(
            host=config.db_host,
            user=config.db_user,
            passwd=config.db_pass,
            database=config.db_database
        )
    def unesiodeljenje(self,odeljenje):
        db = self.__getconnection()
        cur = db.cursor()
        cur.execute(f"insert into odeljenja (naziv) values ('{odeljenje.naziv}')")
        odeljenje.id = cur.getlastrowid()
        db.commit() 
        db.close()
    def izmeniodeljenje(self,odeljenje):
        db = self.__getconnection()
        cur = db.cursor()
        cur.execute(f"update odeljenja set naziv='{odeljenje.naziv}' where id = {odeljenje.id}") 
        db.commit() 
        db.close()
    def brisiodeljenje(self,odeljenje):
        db = self.__getconnection()
        cur = db.cursor()
        cur.execute(f"delete from odeljenja where id = {odeljenje.id}") 
        db.commit() 
        db.close()
    def uzmiodeljenje(self,oid):
        db = self.__getconnection()
        cur = db.cursor()
        cur.execute(f"select * from odeljenja where id = {oid}") 
        jedan = cur.fetchone()
        res = hospital.odeljenje.Odeljenje(jedan[0],jedan[1])
        db.close()
        return res
    def svaodeljenja(self,filter=""):
        db = self.__getconnection()
        cur = db.cursor()
        cur.execute(f"select * from odeljenja") 
        res = []
        for jedan in cur.fetchall():
            res.append(hospital.odeljenje.Odeljenje(jedan[0],jedan[1]))
        db.close()
        return res