from hospital.odeljenje import Odeljenje

class UI:

    NOVO_ODELJENJE      = 1
    IZMENA_ODELJENJA    = 2
    BRISANJE_ODELJENJA  = 3
    SVA_ODELJENJA       = 4

    EXIT                = 0

    def getint(self,msg):
        return int(input(msg))
    def gettxt(self,msg):
        return input(msg)
    def getodeljenje(self):
        return Odeljenje(self.getint("Id: "),self.gettxt("Naziv: "))
    def izmeniodeljenje(self,odeljenje):
        odeljenje.naziv = self.gettxt("Novi naziv: ")
    def mainmenu(self):
        print("1.novo odeljenje, 2.izmena, 3.brisanje, 4.sva odeljenja, 0. izlaz")
        return self.getint("Unesi komandu: ")
    