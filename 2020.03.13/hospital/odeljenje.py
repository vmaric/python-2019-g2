class Odeljenje:
    def __init__(self,oid,naziv):
        self.id = oid
        self.naziv = naziv

    def __str__(self):
        return f"Odeljenje: {self.id} {self.naziv}"