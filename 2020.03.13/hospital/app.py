from hospital.database import Database
from hospital.odeljenje import Odeljenje
from hospital.ui import UI

class Application:
    def __init__(self):
        self.db = Database()
        self.ui = UI()
    
    def start(self):
        while True:
            command = self.ui.mainmenu()
            if command == UI.NOVO_ODELJENJE:
                self.db.unesiodeljenje(self.ui.getodeljenje())
            elif command == UI.IZMENA_ODELJENJA:
                o = self.db.uzmiodeljenje(self.ui.getint("Unesi id: "))
                self.ui.izmeniodeljenje(o)
                self.db.izmeniodeljenje(o)
            elif command == UI.BRISANJE_ODELJENJA:
                o = self.db.uzmiodeljenje(self.ui.getint("Unesi id: "))
                self.db.brisiodeljenje(o)
            elif command == UI.SVA_ODELJENJA:
                for odeljenje in self.db.svaodeljenja():
                    print(odeljenje)
            elif command == UI.EXIT:
                print("Bye Bye")
                exit(0)

    @staticmethod
    def run():
        app = Application()
        app.start()
